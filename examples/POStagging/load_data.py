import os
import numpy as np

PATH = "twpos-data-v0.3/oct27.splits"
FOLDS = ["oct27.train", "oct27.dev", "oct27.test"]

# predicates
sentences = set([])
words = set([])
in_sentence = set([])
is_word = []
prev_word = []
has_tag = []
tags = set([])

sentence_id = 0
word_id = 0
prev_word_id = -1

sentences_ = {}

for fold in FOLDS:
    curr_path = os.path.join(PATH, fold)
    sentences_in_fold = set([])
    with open(curr_path) as f:
        for line in f:
            if line == "\n":
                sentences.add(sentence_id)
                sentences_in_fold.add(sentence_id)
                sentence_id += 1
                prev_word_id = -1
            else:
                word, tag = line.strip().split()
                words.add(word_id)
                tags.add(tag)
                is_word.append((word_id, word))
                has_tag.append((word_id, tag))
                in_sentence.add((word_id, sentence_id))

                if sentence_id not in sentences_:
                    sentences_[sentence_id] = [word_id]
                else:
                    sentences_[sentence_id].append(word_id)

                prev_word.append((word_id, prev_word_id))
                prev_word_id = word_id
                word_id += 1
    with open(fold[6:] + ".txt", 'w') as f:
        for sent_id in sentences_in_fold:
            f.write(str(sent_id) + "\n")


with open("sentences.txt", 'w') as f:
    for sent_id in sentences:
        f.write(str(sent_id) + "\n")

with open("words.txt", 'w') as f:
    f.write("-1\n")
    for word_id in words:
        f.write(str(word_id) + "\n")

with open("tags.txt", "w") as f:
    for tag in tags:
        f.write(tag + "\n")

with open("in_sentence.txt", 'w') as f:
    for (word_id, sent_id) in in_sentence:
        f.write(str(word_id) + "\t" + str(sent_id) + "\n")

with open("is_word.txt", 'w') as f:
    f.write("-1\t_D_U_M_M_Y_\n")
    for (word_id, word) in is_word:
        f.write(str(word_id) + "\t" + word + "\n")

with open("has_tag.txt", "w") as f:
    f.write("-1\t*\n")
    for (word_id, tag) in has_tag:
        f.write(str(word_id) + "\t" + tag + "\n")

with open("prev_word.txt", "w") as f:
    for (word_id, prev_word_id) in prev_word:
        f.write(str(word_id) + "\t" + str(prev_word_id) + "\n")

sentence_lengths = [len(sentences_[id_]) for id_ in sentences_]
print "min:", min(sentence_lengths), "max:", max(sentence_lengths), "mean:", np.mean(sentence_lengths), "median:", np.median(sentence_lengths), "std", np.std(sentence_lengths)
