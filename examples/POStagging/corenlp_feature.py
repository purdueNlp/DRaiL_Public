import sys
import os
from nltk.tag.stanford import StanfordPOSTagger

# Stanford Tagger libraries
STANFORD_DIR = "/Users/mlpacheco/Purdue/Research/stanford-postagger-2015-12-09"
MODEL_FILE = os.path.join(STANFORD_DIR, "models/english-bidirectional-distsim.tagger")
JAR_FILE = os.path.join(STANFORD_DIR, "stanford-postagger.jar")

# data files
is_word = os.path.join(sys.argv[1], "is_word.txt")
in_sentence = os.path.join(sys.argv[1], "in_sentence.txt")

# reconstruct sentences
in_sentence_lines = open(in_sentence).readlines()
is_word_lines = open(is_word).readlines()
sentences = {}; ids = {}

output = open(sys.argv[2], 'w')

for i_s, i_w in zip(in_sentence_lines, is_word_lines):
    line = i_s.strip().split()
    sent_id = line[1]; word_id = line[0]
    line = i_w.strip().split()
    word_string = line[1]

    #print "word_id", word_id, "sent_id", sent_id, "word_string", word_string

    if sent_id not in sentences:
        sentences[sent_id] = [word_string]
        ids[sent_id] = [word_id]
    else:
        sentences[sent_id].append(word_string)
        ids[sent_id].append(word_id)

tagger = StanfordPOSTagger(model_filename=MODEL_FILE, path_to_jar=JAR_FILE)

for i in range(0, len(sentences)):
    if str(i) in sentences:
        tags = tagger.tag(sentences[str(i)])
        wids = ids[str(i)]
        for tag, wid in zip(tags, wids):
            output.write(wid + "\t" + tag[1] + "\n")

output.close()
