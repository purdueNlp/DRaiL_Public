import numpy as np
from drail.features.feature_extractor import FeatureExtractor
from drail.features import utils
from collections import OrderedDict
import re
from metaphone import doublemetaphone


class PosFE(FeatureExtractor):

    def __init__(self, w2v_bin_filename = None, twitter_bin_filename = None, pos_bin_filename = None,
                 word_dic={}, train_words = []):
        '''
        additional parameters can be passed to build the extractor
        '''
        super(PosFE, self).__init__()
        self.w2v_bin_filename = w2v_bin_filename
        self.twitter_bin_filename = twitter_bin_filename
        self.pos_bin_filename = pos_bin_filename
        self.word_dic = word_dic
        #self.build()
        self.train_words = train_words
        np.random.seed(17477)

    def build(self):
        print "building"
        # renamed to have the same name as parent class
        # it is a method override
        '''
        1) build word embedding: keep num, size and embeddings
        2) build tag dictionary: keep tags
        '''
        vocabulary = set([]); tags = set([]); sf_tags = set([])
        suffixes = set([]); phonems = set([])
        phonem_freq = {}
        self.vocab_size = None

        for item, curr_tag in self.train_words:
            # word embeddings
            # to-do: create unkonwn vocabulary
            curr_word = self.word_dic[item].lower()

            vocabulary.add(curr_word)
            # add observed suffixes from lengths 1-3
            for i in range(1, 4):
                suffixes.add(curr_word[-i:])

            phonems.add(doublemetaphone(curr_word)[0])
            curr_phonem = doublemetaphone(curr_word)[0]

            if curr_phonem not in phonem_freq:
                phonem_freq[curr_phonem] = {}

            if curr_tag not in phonem_freq[curr_phonem]:
                phonem_freq[curr_phonem][curr_tag] = 1
            else:
                phonem_freq[curr_phonem][curr_tag] = phonem_freq[curr_phonem][curr_tag] + 1

        '''
        if rule_grd.has_body_predicate("HasStanfordTag"):
            ret = rule_grd.get_body_predicates("HasStanfordTag")
            for item in ret:
                sf_tags.add(item['arguments'][1])
        # print sf_tags
        '''

        tags = set(["!", "#", "$", "&", ",", "A", "@", "E", "D", "G", "M", "L", "O", "N", "P", "S", "R", "U", "T", "V", "Y", "X", "Z", "^", "~"])
        self.tag2idx_dic = OrderedDict()


        for i, v in enumerate(tags):
            # print i, v
            self.tag2idx_dic[v] = i
        if self.w2v_bin_filename:
            self.num_word_vectors, self.word_embedding_size, self.word_embedding = utils.embeddings_dictionary(self.w2v_bin_filename)
            print "Using Google W2V: number of words: {}; ".format(self.num_word_vectors), "word dimension: {}".format(self.word_embedding_size)
            pass
        if self.twitter_bin_filename:
            self.num_twitter_words, self.tw_embedding_size, self.tw_word_embedding = utils.embeddings_dictionary_txt(self.twitter_bin_filename)
            print "Using Twitter W2V: number of words: {}; ".format(self.num_twitter_words), "word dimension: {}".format(self.tw_embedding_size)
        if self.pos_bin_filename:
            self.num_pos_vectors, self.pos_embedding_size, self.pos_embedding = utils.embeddings_dictionary(self.pos_bin_filename)
            print "Using POS W2V: number of words: {}; ".format(self.num_pos_vectors), "word dimension: {}".format(self.pos_embedding_size)

        self.num_words, self.onehot_size, self.onehot_vector = utils.onehot_dictionary(vocabulary)
        print "Using onehot: number of words: {}; ".format(self.num_words), "word dimension: {}".format(self.onehot_size)

        self.num_suffix, self.size_suffix, self.suffix_onehot = utils.onehot_dictionary(suffixes)
        print "Using onehot: number of suffixes: {}; ".format(self.num_suffix), "word dimension: {}".format(self.size_suffix)

        self.num_phonem, self.size_phonem, self.phonem_onehot = utils.onehot_dictionary(phonems)
        print "Using onehot: number of phonems: {}; ".format(self.num_phonem), "word dimension: {}".format(self.size_phonem)

        self.num_tags, self.size_tags, self.tag_onehot = utils.onehot_dictionary(tags)
        self.num_sf_tags, self.size_sf_tags, self.sf_tag_onehot = utils.onehot_dictionary(sf_tags)
        # print self.tag_onehot

        # find the most common tag for each phonem
        self.phonem_tag_freq = {}
        for phonem in phonem_freq:
            max_freq = 0; max_tag = None
            for tag in phonem_freq[phonem]:
                if phonem_freq[phonem][tag] > max_freq:
                    max_freq = phonem_freq[phonem][tag]
                    max_tag = tag
            self.phonem_tag_freq[phonem] = max_tag

    # OJO

    def extract_w2v(self, rule_grd):
        if not rule_grd.has_body_predicate("InSentence"):
            return np.random.uniform(-0.0025, 0.0025, self.word_embedding_size)

        words = rule_grd.get_body_predicates("InSentence")
        words = [self.word_dic[w['arguments'][0]].lower() for w in words]
        words = [word for word in words if word != "_D_U_M_M_Y_"]

        ret = np.hstack([self.word_embedding[word] if word in self.word_embedding else np.random.uniform(-0.0025, 0.0025, self.word_embedding_size) for word in words])

        return ret

    def extract_twitter_w2v(self, rule_grd):
        if not rule_grd.has_body_predicate("InSentence"):
            return np.random.uniform(-0.0025, 0.0025, self.tw_embedding_size)

        words = rule_grd.get_body_predicates("InSentence")
        words = [self.preprocess_twitter_word(self.word_dic[w['arguments'][0]].lower()) for w in words]

        ret = []
        for word in words:
            if word in self.tw_word_embedding:
                ret.append(self.tw_word_embedding[word])
            else:
                ret.append(np.random.uniform(-0.0025, 0.0025, self.tw_embedding_size))
        ret = np.hstack(ret)

        return ret

    def extract_pos_w2v(self, rule_grd):
        if not rule_grd.has_body_predicate("HasLabel"):
            return np.random.uniform(-0.0025, 0.0025, self.pos_embedding_size)
        pos = rule_grd.get_body_predicates("HasLabel")
        pos = [p['arguments'][1] for p in pos]

        ret = np.hstack([self.pos_word_embedding[p] if p in self.pos_embedding \
                        else np.random.uniform(-0.0025, 0.0025, self.pos_embedding_size)\
                        for p in pos])

    def extract_bow(self, rule_grd):
        if not rule_grd.has_body_predicate("InSentence"):
            return np.zeros(self.onehot_size)

        words = rule_grd.get_body_predicates("InSentence")
        words = [self.word_dic[w['arguments'][0]].lower() for w in words]

        ret = np.hstack([self.onehot_vector[word] if word in self.onehot_vector else np.random.uniform(-0.0025, 0.0025, self.onehot_size) for word in words])
        return ret

    def extract_phonem(self, rule_grd):
        if not rule_grd.has_body_predicate("InSentence"):
            return np.zeros(self.size_phonem)

        words = rule_grd.get_body_predicates("InSentence")
        words = [self.word_dic[w['arguments'][0]].lower() for w in words]
        phonems = [doublemetaphone(w)[0] for w in words]
        ret = np.hstack([self.phonem_onehot[ph] if ph in self.phonem_onehot else np.zeros(self.size_phonem) for ph in phonems])
        return ret

    def extract_phonem_freq(self, rule_grd):
        if not rule_grd.has_body_predicate("InSentence"):
            return np.zeros(self.size_phonem)

        curr_word = self.word_dic[rule_grd.get_body_predicates("InSentence")[0]['arguments'][0]].lower()
        phonem = doublemetaphone(curr_word)[0]
        curr_tag = rule_grd.get_head_predicate()['arguments'][1]
        if not phonem in self.phonem_onehot:
            return np.asarray([0.0])
        elif curr_tag == self.phonem_tag_freq[phonem]:
            return np.asarray([1.0])
        else:
            return np.asarray([0.0])

    def extract_suffix(self, rule_grd):
        if not rule_grd.has_body_predicate("InSentence"):
            return np.zeros(self.size_suffix)
        words = rule_grd.get_body_predicates("InSentence")
        words = [self.word_dic[w['arguments'][0]].lower() for w in words]
        suffixes = []
        for w in words:
            vector = np.zeros(self.size_suffix)
            for i in range(1, 4):
                suff = w[-i:]
                if suff in self.suffix_onehot:
                    vector = vector + self.suffix_onehot[suff]
            suffixes.append(vector)
        ret = np.hstack([vec for vec in suffixes])
        return ret

    def extract_digits(self, rule_grd):
        if not rule_grd.has_body_predicate("InSentence"):
            return np.zeros(1)

        words = rule_grd.get_body_predicates("InSentence")
        words = [self.word_dic[w['arguments'][0]] for w in words]
        ret = np.hstack([[1.0] if re.match(r'.*\d.*', w) else 0.0 for w in words])
        return ret

    def extract_hyphens(self, rule_grd):
        if not rule_grd.has_body_predicate("InSentence"):
            return np.zeros(1)

        words = rule_grd.get_body_predicates("InSentence")
        words = [self.word_dic[w['arguments'][0]] for w in words]
        ret = np.hstack([[1.0] if re.match(r'.*-.*', w) else 0.0 for w in words])
        return ret


    def extract_cap_patterns(self, rule_grd):
        if not rule_grd.has_body_predicate("InSentence"):
            return np.zeros(4)

        words = rule_grd.get_body_predicates("InSentence")
        words = [self.word_dic[w['arguments'][0]] for w in words]
        ret = np.hstack([self.cap_patters(w) for w in words])
        return ret


    def extract_tw_orth(self, rule_grd):
        if not rule_grd.has_body_predicate("InSentence"):
            return np.zeros(4)

        words = rule_grd.get_body_predicates("InSentence")
        words = [self.word_dic[w['arguments'][0]] for w in words]
        ret = np.hstack([self.tw_orth(w) for w in words])
        return ret

    def extract_tag(self, rule_grd):
        if not rule_grd.has_body_predicate("HasLabel"):
            return np.zeros(self.size_tags)

        tags = rule_grd.get_body_predicates("HasLabel")
        tags = [tag['arguments'][1] for tag in tags]
        ret = np.hstack([self.tag_onehot[tag] if tag in self.tag_onehot else np.zeros(self.size_tags) for tag in tags])
        #print tags, ret
        return ret

    def extract_stanford_tag(self, rule_grd):
        if not rule_grd.has_body_predicate("HasStanfordTag"):
            return np.random.zeros(self.size_sf_tags)

        sf_tags = rule_grd.get_body_predicates("HasStanfordTag")
        sf_tags = [tag['arguments'][1] for tag in sf_tags]

        ret = np.hstack([self.sf_tag_onehot[tag] if tag in self.sf_tag_onehot else np.zeros(self.size_sf_tags) for tag in sf_tags])
        return ret

    def extract_multiclass_head(self, rule_grd):
        ret = rule_grd.get_head_predicate()
        # print self.tag2idx_dic[ret['arguments'][1]]

        return self.tag2idx_dic[ret['arguments'][1]]

    def preprocess_twitter_word(self, word):
        eyes = "[8:=;]"
        nose = "['`\-]?"
        nw = re.sub(r'https{0,1}://\S+\b|www\.(\w+\.)+\S*/', "<url>", word)
        nw = re.sub(r'@\w+', "<user>", word)
        nw = re.sub(eyes + nose + r'[)d]+|[)d]+' + nose + eyes, "<smile>", word)
        nw = re.sub(eyes + nose + r'p+', "<lolface>", word)
        nw = re.sub(eyes + nose + r'\(+|\)+' + nose + eyes, "<sadface>", word)
        nw = re.sub(eyes + nose + r'[\/|l*]', "<neutralface>", word)
        nw = re.sub(r'<3', "<heart>", word)
        nw = re.sub(r'[-+]?[.\d]*[\d]+[:,.\d]*', "<number>", word)
        nw = re.sub(r'#\S+', "<hashtag>", word)
        nw = re.sub(r'([!?.]){2,}', "<repeat>", word)
        nw = re.sub(r'\b(\S*?)(.)\2{2,}\b', "<elong>", word)
        return nw

    def cap_patters(self, word):
        ret = []
        if re.match(r'^[A-Z][a-z]+$', word):
            ret = [1.0, 0.0, 0.0, 0.0]
        elif re.match(r'^[A-Z]+$', word):
            ret = [0.0, 1.0, 0.0, 0.0]
        elif re.match(r'[a-z]+', word):
            ret = [0.0, 0.0, 1.0, 0.0]
        else:
            ret = [0.0, 0.0, 0.0, 1.0]
        return np.asarray(ret)

    def tw_orth(self, word):
        if re.match(r'@\w+', word):
            ret = [1.0, 0.0, 0.0]
        elif re.match(r'#\S+', word):
            ret = [0.0, 1.0, 0.0]
        elif re.match(r'https{0,1}://\S+\b|www\.(\w+\.)+\S*/', word):
            ret = [0.0, 0.0, 1.0]
        else:
            ret = [0.0, 0.0, 0.0]
        return np.asarray(ret)
