import json

has_label = open("data/has_label.txt", "w")
in_instance = open("data/in_instance.txt", "w")
items = open("data/items.txt", "w")
features = {}

labels = set([])

with open("data/iris.txt") as fp:
    for i, line in enumerate(fp):
        elems = line.strip().split(",")
        items.write("{0}\n".format(i))
        has_label.write("{0}\t{1}\n".format(i, elems[-1]))
        in_instance.write("{0}\t0\n".format(i))
        labels.add(elems[-1])
        features[i] = map(float, elems[0:-1])

has_label.close()
items.close()

with open("data/labels.txt", "w") as fp:
    for label in labels:
        fp.write("{0}\n".format(label))


with open("data/features.json", "w") as fp:
    json.dump(features, fp)

with open("data/instances.txt", "w") as fp:
    fp.write("0\n")
