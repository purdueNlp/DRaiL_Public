import json
import numpy as np

from drail.features.feature_extractor import FeatureExtractor


class IrisFE(FeatureExtractor):

    def __init__(self, features_fname=None):
        self.features_fname = features_fname

    def build(self):
        if self.features_fname:
            with open(self.features_fname) as fp:
                self.features = json.load(fp)

        self.label2idx_dic = \
            {"Iris-virginica": 0,
             "Iris-setosa": 1,
             "Iris-versicolor": 2}

    def extract_all(self, rule_grd):
        if not rule_grd.has_body_predicate("InInstance"):
            return np.zeros(4)

        pred = rule_grd.get_body_predicates("InInstance")[0]
        item = pred['arguments'][0]
        ret = np.array(self.features[str(item)])
        #print ret
        return ret

    def extract_multiclass_head(self, rule_grd):
        ret = rule_grd.get_head_predicate()
        return self.label2idx_dic[ret['arguments'][1]]

 
