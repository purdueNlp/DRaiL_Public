import sys
import os
import numpy as np
import torch
import optparse
from sklearn.metrics import *
import random
import glob

from drail.learn.local_learner import LocalLearner


def main():
    BASE_DIR = "examples/SimpleClassifier/"


    data_dir = os.path.join(BASE_DIR, "data")
    rule_file = os.path.join(BASE_DIR, "rule.dr")
    conf_file = os.path.join(BASE_DIR, "config.json")
    feature_file = os.path.join(BASE_DIR, "data", "features.json")

    num_samples = 150
    ids = range(0, num_samples)
    random.shuffle(ids)

    # dividing splits 0.8 training, 0.1 for test and 0.1 for dev
    train_ids = ids[:120]
    dev_ids = ids[120:135]
    test_ids = ids[135:]

    learner = LocalLearner()

    learner.compile_rules(rule_file)
    db = learner.create_dataset(data_dir)

    # This is a way to programatically and dinamically add filters over the dataset as we go
    # it can be useful to not redefine a rule file when we use K-fold cross validation
    # Filters are over predicates and take the form
    # (PredicateName, FilterName, ArgumentName, FilterValues)
    db.add_filters(
      filters=[("Item", "isTrain", "itemId", train_ids),
               ("Item", "isDev", "itemId", dev_ids),
               ("Item", "isTest", "itemId", test_ids)])

    learner.build_feature_extractors(
            db, features_fname=feature_file,
            femodule_path=BASE_DIR)

    learner.build_models(db, conf_file)

    # Specify what to filter for train, dev etc when training neural nets
    learner.train(db,
                  train_filters=[("X", "isTrain", 1)],
                  dev_filters=[("X", "isDev", 1)],
                  test_filters=[("X", "isTest", 1)])

    # We can call predict but inference will just be argmax over nnet scores 
    # since there is just one rule with a local mapping item->label
    # results should match the above ones

    learner.extract_data(
            db,
            test_filters=[("X", "isTest", 1)])

    res = learner.predict(db, fold='test')

    y_gold = res.metrics['HasLabel']['gold_data']
    y_pred = res.metrics['HasLabel']['pred_data']

    # any sklearn metrics can be used over y_gold and y_pred
    acc_test = accuracy_score(y_gold, y_pred)
    print classification_report(y_gold, y_pred)
    print "TEST acc", acc_test
    learner.reset_metrics()

    for zippath in glob.iglob(os.path.join(".", '*.pt')):
        os.remove(zippath)

if __name__ == '__main__':
    main()
