import sys
import os
import numpy as np
import torch
import optparse
from sklearn.metrics import *

from drail.learn.local_learner import LocalLearner
from drail.learn.global_learner import GlobalLearner

#torch.cuda.set_device(0)

def get_fold_ids(_dir, fold):
    filter_ids = []
    curr_path = os.path.join(_dir, '{0}.txt'.format(fold))
    with open(curr_path) as f:
        for line in f:
            filter_ids.append(line.strip())
    return filter_ids

def build_word_dic(filename):
    word_dic = {}
    with open(filename) as f:
        for line in f:
            wordid, word = line.strip().split()
            word_dic[int(wordid)] = word
    return word_dic

def get_train_words(filename_sent,
                    filename_tags, train_ids):
    train_ids = set(train_ids)
    train_words = []
    with open(filename_sent) as f:
        for line in f:
            wordid, sentenceid = line.strip().split()
            if sentenceid in train_ids:
                train_words.append(int(wordid))

    train_words = set(train_words)
    ret = []
    with open(filename_tags) as f:
        for line in f:
            wordid, tag = line.strip().split()
            if int(wordid) in train_words:
                ret.append((int(wordid), tag))
    return ret

def main():
    parser = optparse.OptionParser()
    parser.add_option('-d', '--dir', help='directory', dest='dir', type='string')
    parser.add_option('-r', '--rule', help='rule file', dest='rules', type='string',
                       default='rule.dr')
    parser.add_option('-c', '--config', help='config file', dest='config', type='string',
                       default="config.json")
    parser.add_option('-m', help='mode: [global|local]', dest='mode', type='string',
                      default='local')
    parser.add_option('--delta', help='loss augmented inferece', dest='delta', action='store_true',
                      default=False)
    parser.add_option('--lr', help='global learning rate', dest='global_lr', type='float')
    parser.add_option('--te', help='twitter embedding',
                      dest='twitter_w2vbin_filename', default=None)
    parser.add_option('--we', help='word embedding',
                      dest='w2vbin_filename', default=None)
    parser.add_option('--pe', help='POS embedding',
                      dest='pos_w2vbin_filename', default=None)
    (opts, args) = parser.parse_args()

    mandatories = ['dir']
    for m in mandatories:
        if not opts.__dict__[m]:
            print "mandatory option is missing\n"
            parser.print_help()
            exit(-1)

    if opts.mode == "global" and not opts.__dict__['global_lr']:
        print "specify learning rate for global model\n"
        parser.print_help()
        exit(-1)

    if opts.mode not in ['global', 'local']:
        print "specify a valid mode\n"
        parser.print_help()
        exit(-1)

    rule_file = os.path.join(opts.dir, opts.rules)
    conf_file = os.path.join(opts.dir, opts.config)
    train_ids = get_fold_ids(opts.dir, 'train')
    dev_ids = get_fold_ids(opts.dir, 'dev')
    test_ids = get_fold_ids(opts.dir, 'test')
    word_dic = build_word_dic(os.path.join(opts.dir, "is_word.txt"))

    train_words = get_train_words(os.path.join(opts.dir, "in_sentence.txt"),
                                  os.path.join(opts.dir, "has_tag.txt"),
                                  train_ids)

    np.random.seed(123)

    ## Now use learner class here
    if opts.mode == "global":
       learner = GlobalLearner(learning_rate=0.06)
    else:
       learner = LocalLearner()

    print "Compiling rules..."
    learner.compile_rules(rule_file)

    print "Loading dataset..."
    db = learner.create_dataset(opts.dir)

    # add splits
    db.add_filters(filters=[
        ("Sentence", "isTrain", "sentenceId", train_ids),
        ("Sentence", "isDev", "sentenceId", dev_ids),
        ("Sentence", "isTest", "sentenceId", test_ids)
        ])

    print "Building feature extractors..."
    learner.build_feature_extractors(
            db, filters=[("Y", "isTrain", 1)],
            w2v_bin_filename=opts.w2vbin_filename,
            twitter_bin_filename=opts.twitter_w2vbin_filename,
            pos_bin_filename=opts.pos_w2vbin_filename,
            word_dic=word_dic,
            train_words=train_words,
            femodule_path=opts.dir)

    print "Building neural nets..."
    learner.build_models(db, conf_file)

    if opts.mode == "global":
        learner.extract_data(
                db,
                train_filters=[("Y", "isTrain", 1)],
                dev_filters=[("Y", "isDev", 1)],
                test_filters=[("Y", "isTest", 1)])
        res = learner.train(
                db,
                train_filters=[("Y", "isTrain", 1)],
                dev_filters=[("Y", "isDev", 1)],
                test_filters=[("Y", "isTest", 1)],
                opt_predicate='HasLabel',
                hot_start=False,
                loss_augmented_inference=opts.delta,
                K=2)
    else:
        _ = learner.train(
                db,
                train_filters=[("Y", "isTrain", 1)],
                dev_filters=[("Y", "isDev", 1)],
                test_filters=[("Y", "isTest", 1)])
        # locally we only need inference data for prediction
        learner.extract_data(
                    db,
                    test_filters=[("Y", "isTest", 1)])
        res = learner.predict(
                    db,
                    fold='test', K=2)

    y_gold = res.metrics['HasLabel']['gold_data']
    y_pred = res.metrics['HasLabel']['pred_data']

    # any sklearn metrics can be used over y_gold and y_pred
    acc_test = accuracy_score(y_gold, y_pred)
    print classification_report(y_gold, y_pred)
    print "TEST acc", acc_test
    learner.reset_metrics()

if __name__ == '__main__':
    main()
