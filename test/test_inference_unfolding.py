import sys
import os
from drail.database import Database
from drail.parser import parser


def main():
    if len(sys.argv) < 3:
        sys.exit("Usage:" + " python " + sys.argv[0] +" [rule file] [data path] [filter_file1, filter_file2, ...]")
    #fout = open("testresult.txt", 'w')

    filename = sys.argv[1]
    path = sys.argv[2]
    text = open(filename).read()
    par = parser.Parser()
    par.build()
    par.parse(filename)

    filter_instances = []
    for i in range(3, len(sys.argv)):
        with open(sys.argv[i]) as f:
            for line in f:
                filter_instances.append(line.strip())

    # Uncomment to see output of parser
    print "predicates", par.predicate_arguments
    print "labels", par.label_types.keys()
    print "files", par.files
    print "rulesets", par.rulesets
    print "groupby", par.groupby

    db = Database()
    # Use your own path

    db.load_predicates(par.predicate_arguments, path, par.files)
    db.load_labels(par.label_types.keys(), path, par.files)
    print db.table_index

    ruleset = par.rulesets[0]
    group_by = par.groupby[0]

    # this will return the ids that can then be used to query the database
    # to group ILP problems
    instances = db.get_ruleset_instances(ruleset['predict'], group_by, filter_by=(group_by, filter_instances))

    for i in instances:
        instance_groundings = []; constraint_groundings = []
        #if i != 10:
        #    continue

        #fout.write("\ninstance: " + str(i))
        print "\ninstance " + str(i)
        for rule_template in ruleset['predict']:
            print "Rule", rule_template
            rule_groundings = db.unfold_rule_groundings(rule_template, group_by, i)
            #fout.write(str(rule_groundings) + "\n")
            instance_groundings += rule_groundings

        for rule_template in ruleset['constr']:
            print "Constraint", rule_template
            rule_groundings = db.unfold_rule_groundings(rule_template, group_by, i, True)
            #fout.write(str(rule_groundings) + "\n")
            constraint_groundings += rule_groundings

        print "Instance groundings"
        for gr in instance_groundings:
            print gr


        print "Constraint groundings"
        for gr in constraint_groundings:
            print gr
        break
        #fout.write("\n")
    #fout.close()


if __name__ == "__main__":
    main()

