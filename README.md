## Installing Requirements
- gurobi optimizer (academic license can be requested)
- gurobipy (follow gurobi instructions, can be installed from package)
- `pip install -r requirements.txt`

## Running demo
- Download [Twitter Glove Embeddings](http://nlp.stanford.edu/data/glove.twitter.27B.zip)
- `python run_pos.py -d examples/POStagging/ -r rule.dr -c config.json -m local --te [GloVe Twitter embedding txt file]` to run Local Version
- `python run_pos.py -d examples/POStagging/ -r rule.dr -c config.json -m global --lr [learning rate] --te [GloVe Twitter embedding txt file]` to run Global Version

