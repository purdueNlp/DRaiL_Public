import torch
from torch.autograd import Variable
import numpy as np
from sklearn.metrics import *
import sys
import time
from collections import Counter
import time

from nn_model import ModelType

def compute_weights(Y, output_dim):
    counts = np.bincount(Y)
    num_missing = output_dim - len(counts)
    counts = np.append(counts, np.asarray([0]*num_missing))

    # smoothing
    counts[counts == 0] = 1
    #compute weights
    weights = np.asarray(np.max(counts)*1.0/counts, dtype = "float32")
    return weights
    #eg_weights = [weights[Y[idx]] for idx in range(len(Y))]
    #return np.asarray(eg_weights, dtype='float32')

def measure(y, y_, measurement, average, pos_label):
    if measurement == "acc":
        return accuracy_score(y, y_)
    elif measurement == "f1":
        return f1_score(y, y_, average = average, pos_label= pos_label)

def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
    if shuffle:
        indices = np.arange(len(targets))

    end_idx = max(len(targets) - batchsize + 1, 1)
    # print indices
    ret = {}

    for start_idx in range(0, end_idx, batchsize):
        # print batchsize
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsize]
        else:
            excerpt = slice(start_idx, start_idx + batchsize)

        ret['vector'] = [inputs['vector'][i] for i in excerpt]
        ret['embedding'] = {}
        for emb in inputs['embedding']:
            ret['embedding'][emb] = [inputs['embedding'][emb][i] for i in excerpt]

        yield ret, [targets[i] for i in excerpt]

def sort_batch(x, y, length):
    batch_size = x.size(0)                       # get size of batch
    sorted_length, sorted_idx = length.sort()    # sort the length of sequence samples
    reverse_idx = torch.linspace(batch_size-1,0,batch_size).long().cuda()
    sorted_length = sorted_length[reverse_idx]    # for descending order
    sorted_idx    = sorted_idx[reverse_idx]
    sorted_data   = x[sorted_idx]                 # sorted in descending order
    sorted_labels = y[sorted_idx]
    return sorted_data, sorted_labels, sorted_length

# FIX with gpu flag if we want to use again
def padding_helper(X, Y):
    input_lengths = torch.cuda.LongTensor(map(len, X))
    target_Y = torch.cuda.LongTensor(Y)
    input_X = torch.zeros((len(X), input_lengths.max())).long().cuda()
    for idx, (seq, seq_len) in enumerate(zip(X, input_lengths)):
         input_X[idx, :seq_len] = torch.cuda.LongTensor(seq)
    return sort_batch(input_X, target_Y, input_lengths)

def train_local(model, optimizer, loss_fn, X, Y, n_batchsize, class_weights,
          measure_type='acc', avg_type='binary', pos_label=1):

    train_batches = 0; train_loss_sum = 0.0; train_measure_sum = 0.0
    model.train()
    for (batched_X, batched_Y) in \
         iterate_minibatches(X, Y, n_batchsize, shuffle=True):

        model.zero_grad()
        y_pred = model(batched_X)
        _, train_Y_pred = torch.max(y_pred, 1)

        train_loss = model.global_loss(batched_X, train_Y_pred.data.long(), batched_Y)

        # Compute and accumulate loss
        #train_loss = model.pytorch_loss(y_pred, batched_Y, loss_fn)
        train_loss_sum += train_loss.data[0]

        train_batches += 1

        # Zero gradients, perform a backward pass, and update the weights.
        train_loss.backward()
        optimizer.step()
    #print "---"
    return train_batches, train_loss_sum, train_measure_sum

def get_sequence_length(X):
    _max = len(X['vector'])
    for emb in X['embedding']:
        _max = max(_max, len(X['embedding'][emb]))
    return _max

def predict_local(model, X):
    model.eval()
    if model.type == ModelType.Sequence:
        #X, Y, input_lengths = padding_helper(X, Y)
        y_pred = []
        seq_len = get_sequence_length(X)
        for i in range(seq_len):
            model.minibatch_size = 1
            model.hidden = model.init_hidden()
            output = model(X)
            _, y_pred_i = torch.max(output, 1)
            y_pred.append(y_pred_i.data[0])
        return np.asarray(y_pred)
    else:
        output = model(X)
        _, y_pred = torch.max(output, 1)
        return (y_pred.data).cpu().numpy()

def predict_local_scores(model, X):
    model.eval()
    if model.type == ModelType.Sequence:
        #X, Y, input_lengths = padding_helper(X, Y)
        output = np.asarray([])
        seq_len = get_sequence_length(X)
        for i in range(seq_len):
            model.minibatch_size = 1
            model.hidden = model.init_hidden()
            output_i = model(X)
            if output.shape[0] == 0:
                output = (output_i.data).cpu().numpy()
            else:
                output = np.vstack([output, (output_i.data).cpu().numpy()])
        return np.asarray(output)
    else:
        output = model(X)
        return (output.data).cpu().numpy()

def backpropagate_global(model, optimizer, X_ls, y_pred_ls, y_pred_index_ls,
                        y_gold_ls, y_gold_index_ls):
    model.train()
    model.zero_grad()
    loss = model.loss(X_ls, y_pred_ls, y_pred_index_ls, y_gold_ls, y_gold_index_ls)
    loss.backward()
    optimizer.step()
    return loss.data[0]

def train_local_on_epoches(model, TrainX, TrainY, DevX, DevY, TestX, TestY):
    timestamp = time.time()
    print("Neural Networks {} Training Started..".format(model.nn_id))
    patience = model.config["patience"]
    n_batchsize = model.config["batch_size"]

    # defaults. TO-DO add defaults for the rest of the params
    measurement = "acc"; average = "binary"; weighted_examples=False
    pos_label = 1

    if "measurement" in model.config:
        measurement = model.config["measurement"]
    if "average" in model.config:
        average = model.config["average"]
    if "weighted_examples" in model.config:
        weighted_examples = model.config["weighted_examples"]
    print_stat = measurement
    if "print_stat" in model.config:
        print_stat = model.config['print_stat']
    if "pos_label" in model.config:
        pos_label = int(model.config["pos_label"])

    # print n_batchsize
    best_val_measure = -float("infinity")
    done_looping = False
    patience_counter = 0

    epoch = 0
    start_time = time.time()

    if weighted_examples:
        class_weights = torch.from_numpy(compute_weights(TrainY, model.output_dim))
        if model.use_gpu:
            class_weights = class_weights.cuda()
    else:
        class_weights = None

    # THIS IS HARDCODED, NEED TO ALLOW FOR DIFFERENT OPT AND LOSSES
    #loss_fn = torch.nn.CrossEntropyLoss(weight=class_weights)
    loss_fn = None
    optimizer = torch.optim.SGD(model.parameters(),
                                lr=float(model.config['learning_rate']))
    while (not done_looping):
    #for i in range(5):
        epoch += 1
        train_batches, train_loss_sum, train_measure_sum = \
                train_local(model, optimizer, loss_fn,
                            TrainX, TrainY, n_batchsize, class_weights,
                            measurement, average, pos_label)
        if len(DevX) == 0:
            val_measure = 0.0
        else:
            val_Y_pred = predict_local(model, DevX)
            val_measure = measure(DevY, val_Y_pred, measurement, average, pos_label)

        # report metrics
        if len(TrainX) == 0:
            train_measure = 0.0
        else:
            train_Y_pred = predict_local(model, TrainX)
            train_measure = measure(TrainY, train_Y_pred, measurement, average, pos_label)

        if TestX is not None and TestY is not None:

            if len(TestX) == 0:
                test_measure = 0.0
            else:
                test_Y_pred = predict_local(model, TestX)
                test_measure = measure(TestY, test_Y_pred, measurement, average, pos_label)
        else:
            pass
        '''
        #print "--- TRAIN SCORE", train_measure
        '''
        #print "epoch", epoch, "loss", train_loss_sum / (1.0 * train_batches), "train_measure", train_measure, "val_measure", val_measure
        if val_measure > best_val_measure:
            print "epoch", epoch, "loss", train_loss_sum / (1.0 * train_batches), "train_measure", train_measure, "val_measure", val_measure
            patience_counter = 0
            best_val_measure = val_measure
            best_epoch = epoch
            model_state = model.state_dict()
            torch.save(model_state, 'best_model_{0}.pt'.format(timestamp))
        else:
            patience_counter += 1
            if patience_counter >= patience:
                done_looping = True

    model.load_state_dict(torch.load('best_model_{0}.pt'.format(timestamp)))

    dev_Y_pred = predict_local(model, DevX)
    train_Y_pred = predict_local(model, TrainX)
    test_Y_pred = predict_local(model, TestX)

    '''
    print "\nTraining scores"
    print classification_report(TrainY, train_Y_pred)
    print "f1 macro", f1_score(TrainY, train_Y_pred, average='macro')
    #print confusion_matrix(TrainY, train_Y_pred)
    print "\nDev scores"
    #print classification_report(DevY, dev_Y_pred)
    print "f1 macro", f1_score(DevY, dev_Y_pred, average='macro')
    #print confusion_matrix(DevY, dev_Y_pred)
    '''
    if TestX is not None and TestY is not None:
        print "\tTest scores"
        print classification_report(TestY, test_Y_pred)
        print "f1 macro", f1_score(TestY, test_Y_pred, average='macro')
    print("\nTraining {} Epoches took {:.3f}s".format(epoch, time.time() - start_time))
    '''
    if TestX is not None and TestY is not None:
        return [measure(TrainY, train_Y_pred, print_stat, average, pos_label),
                measure(DevY, dev_Y_pred, print_stat, average, pos_label),
                measure(TestY, test_Y_pred, print_stat, average, pos_label),
                confusion_matrix(TestY, test_Y_pred)]
    else:
        return [measure(TrainY, train_Y_pred, print_stat, average, pos_label),
                measure(DevY, dev_Y_pred, print_stat, average, pos_label),
                None,
                confusion_matrix(TestY, test_Y_pred)]
    '''
    return None
